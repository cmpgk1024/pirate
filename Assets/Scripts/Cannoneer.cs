﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Cannoneer : MonoBehaviour {
	public GameObject Cannonball;
	public float shotDelay = 3f;
	public float shotForce = 100;
	public Transform cannon;
	public Vector3 cannonballPos;
	public float aimOffset = -11.55342f;
	Transform aimCamera;
	public GameObject fuse;
	float nextShot = Mathf.Infinity; //time.time will never be infinity
	float rotationX = 90;
	void Start(){
		aimCamera = transform.Find("Aim Camera");
	}
	void Update () {
		if(Input.GetKeyDown(KeyCode.Space)){
			nextShot = Time.time + shotDelay;
			transform.Find("Camera").gameObject.SetActive(false);
			aimCamera.gameObject.SetActive(true);
			fuse.SetActive(true);
		}	
		if(Input.GetKey(KeyCode.Space)){
			fuse.transform.localScale = new Vector3(0.05f, nextShot - Time.time, 1f);
			//cannon.Rotate(new Vector3(-Input.GetAxisRaw("Vertical") / 10, 0, 0));
			rotationX += -Input.GetAxisRaw("Vertical");
			rotationX = Mathf.Clamp(rotationX, 86, 90);
			cannon.localEulerAngles = new Vector3(rotationX, cannon.localEulerAngles.y, 0);
		}
		if(Time.time > nextShot || (Input.GetKeyUp(KeyCode.Space) && Time.time < nextShot && nextShot != Mathf.Infinity)){
			nextShot = Mathf.Infinity;
			Debug.Log("shooting");
			transform.Find("Camera").gameObject.SetActive(true);
			fuse.SetActive(false);
			aimCamera.gameObject.SetActive(false);
			GameObject cball = Network.Instantiate(Cannonball, (transform.position + cannonballPos),
			 Quaternion.identity, 0) as GameObject;
			cball.rigidbody.AddForce(cannon.transform.up * shotForce);
		}
	}
}
