﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Enemy : MonoBehaviour {
	enum State{
		Idle,
		Moving
	};
	enum Side{
		Undecided,
		Right,
		Left
	};
	State state = State.Idle;
	Side approachSide = Side.Undecided;
	GameObject player;
	public int Health = 100;
	public float mastOutSpeed = 15;
	public float shotDelay = 5f;
	public float nextShot = 0f;
	public GameObject cannonball;
	NavMeshAgent agent;
	bool canShoot = false;
	void Start(){
		player = GameObject.Find("Boat");
		agent = GetComponent<NavMeshAgent>();
	}
	void Update () {
		if(Health <= 0)
			Destroy(gameObject);
		switch(state){
			case State.Idle:
				RaycastHit hit;
				Vector3 rayDirection = player.transform.position - transform.position;
				if (Physics.Raycast (transform.position, rayDirection, out hit)) {
					if(hit.transform == player.transform)
						state = State.Moving;
				}
				break;
			case State.Moving:
				Vector3 relativePoint = player.transform.InverseTransformPoint(transform.position);
				if(relativePoint.x < 0.0)
					approachSide = Side.Left;
				else if(relativePoint.x > 0.0)
					approachSide = Side.Right;
				else{
					approachSide = Side.Left;
				}
				switch(approachSide){
					case Side.Left:
						agent.SetDestination(player.transform.position + player.transform.forward * 30 + player.transform.up * 10);
					break;
					case Side.Right:
						agent.SetDestination(player.transform.position - player.transform.forward * 30 - player.transform.up * 10);
					break;
				}
				if(Vector3.Distance(transform.position, player.transform.position) < 31){
					canShoot = true;
				}
				else{
					canShoot = false;
				}
				if(canShoot && Time.time > nextShot){
					nextShot = Time.time + shotDelay;
					GameObject cball = Network.Instantiate(cannonball, transform.position, Quaternion.identity, 0) as GameObject;
					cball.rigidbody.AddForce(transform.
				}
				break;
		}
	}
	public void MastHit(){
		GetComponent<NavMeshAgent>().speed = mastOutSpeed;
	}
	public void HullHit(){
		Health -= 25;
	}
}
