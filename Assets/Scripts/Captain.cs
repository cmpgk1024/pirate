﻿using UnityEngine;
using System.Collections;

public enum ZoomDir{
	In,
	Out
};

public class Captain : MonoBehaviour {
	public bool zoomed = false;
	void Update () {
		if(Input.GetButtonDown("Fire1")){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit)){
				Debug.Log (hit.transform.gameObject.name);
				if(hit.transform.gameObject.name == "map")
					MapZoom(ZoomDir.In);
			}
		}
	}
	public void MapZoom(ZoomDir dir){
		switch(dir){
		case ZoomDir.In:
			zoomed = true;
			GameObject.Find("Map Camera").camera.ResetAspect();
			GameObject.Find("Map Camera").camera.depth = 1;
			break;
		case ZoomDir.Out:
			zoomed = false;
			GameObject.Find("Map Camera").camera.aspect = 1f;
			GameObject.Find("Map Camera").camera.depth = -1; // move camera back behind
			break;
		}
	}
}
