﻿using UnityEngine;
using System.Collections;

public class Helmsman : MonoBehaviour {
	
	public Transform boat;

	void Update () {
		transform.parent.GetComponent<Boat>().Steer (new Vector3 (0, Input.GetAxisRaw ("Horizontal") / 2));
		Transform wheel = boat.Find("Wheel");
		wheel.Rotate(new Vector3(Input.GetAxisRaw("Horizontal") * 2, 0, 0));
	}
}
