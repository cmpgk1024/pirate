﻿public enum Role{
	None, //uninitialized enum evaluates to the first value, hacky fix
	Helmsman,
	Cannoneer,
	Lookout,
	Captain
}