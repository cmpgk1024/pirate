﻿using UnityEngine;
using System.Collections;

public class RenderTextureTarget : MonoBehaviour {

	public RenderTexureFree renderTexture;
	public float updateInterval;
	float nextUpdate = 0;
	
	// Update is called once per frame
	void Update () {
		if(Time.time > nextUpdate){
			UpdateTex();
		}
	}
	
	void UpdateTex(){
		renderer.material.mainTexture = renderTexture.target;
	}
}
