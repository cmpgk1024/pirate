﻿using UnityEngine;
using System.Collections;

public class HullCollider : MonoBehaviour {
	void OnCollisionEnter(Collision c){
		transform.parent.SendMessage("HullHit");
	}
}
