﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PirateNetworkManager : NetworkManager {
	bool selectingRole = false;
	bool gameStarted = false;
	Role role;
	List<Role> availableRoles = new List<Role> () {Role.Cannoneer, Role.Captain, Role.Helmsman, Role.Lookout};
	Dictionary<char,string> serverNames = new Dictionary<char, string>{
		{'a', "Army"},
		{'b', "Batallion"},
		{'c', "Crew"},
		{'d', "Detatchment"},
		{'e', "Enclave"},
		{'f', "Faction"},
		{'g', "Gang"},
		{'h', "Herd"},
		{'i', "Incorporation"},
		{'j', "Joint"},
		{'k', "Klan"},
		{'l', "Lemon"},
		{'m', "Mob"},
		{'n', "Noodlers"},
		{'o', "Organization"},
		{'p', "Party"},
		{'q', "Queens"},
		{'r', "Retinue"},
		{'s', "Sect"},
		{'t', "Team"},
		{'u', "Undertakers"},
		{'v', "Vagabonds"},
		{'w', "Wenches"},
		{'x', "Xylophones"},
		{'y', "Yellowtails"},
		{'z', "Zebras"}
	};
	int playersReady = 0;
	public GameObject boat;
	protected override void OnGUI(){
		base.OnGUI ();
		if (selectingRole) {
			if(availableRoles.Contains(Role.Captain)){
				if(GUI.Button(new Rect(btnX, btnY, btnH, btnW), "Captain")){
					role = Role.Captain;
					networkView.RPC("RemoveRole", RPCMode.AllBuffered, (int)role);
					SelectRole(role);
					selectingRole = false;
				}
			}
			if(availableRoles.Contains(Role.Cannoneer)){
				if(GUI.Button(new Rect(btnX, btnY * 1.1f + btnH, btnH, btnW), "Cannoneer")){
					role = Role.Cannoneer;
					networkView.RPC("RemoveRole", RPCMode.AllBuffered, (int)role);
					SelectRole(role);
					selectingRole = false;
				}
			}
			if(availableRoles.Contains(Role.Helmsman)){
				if(GUI.Button(new Rect(btnX, btnY * 1.2f + btnH * 2f, btnH, btnW), "Helmsman")){
					role = Role.Helmsman;
					networkView.RPC("RemoveRole", RPCMode.AllBuffered, (int)role);
					SelectRole(role);
					selectingRole = false;
				}
			}
			if(availableRoles.Contains(Role.Lookout)){
				if(GUI.Button(new Rect(btnX, btnY * 1.3f + btnH * 3f, btnH, btnW), "Lookout")){
					role = Role.Lookout;
					networkView.RPC("RemoveRole", RPCMode.AllBuffered, (int)role);
					SelectRole(role);
					selectingRole = false;
				}
			}
		}
		try{
			serverName = userName + "'s " + serverNames[userName.ToLower()[0]];
		}
		catch(System.Exception e){
			serverName = "A Pirate Crew";
		}
	}
	protected override void OnConnectedToServer(){
		base.OnConnectedToServer ();
		selectingRole = true;
	}

	protected override void OnServerInitialized(){
		base.OnServerInitialized ();
		selectingRole = true;
	}

	void SelectRole(Role r){
		boat.transform.Find ("DeathCam").gameObject.SetActive (false);
		if(r != Role.Captain)
			boat.transform.Find (r.ToString()).gameObject.SetActive (true);
		else
			GameObject.Find("CaptainRoot").transform.Find(r.ToString()).gameObject.SetActive(true);
		if (Network.isClient)
			networkView.RPC ("ReadyPlayer", RPCMode.Server);
		/*else if (Network.isServer) server is not counted in Network.Connections.Length, so this is unecessary
			playersReady++;*/
	}

	[RPC]
	void RemoveRole(int r){
		availableRoles.Remove ((Role)r);
	}

	[RPC]
	void ReadyPlayer(){
		playersReady++;
	}

	protected override void Update(){
		if(refreshing){
			if(MasterServer.PollHostList().Length > 0){
				refreshing = false;
				Debug.Log("Number of hosts: " + MasterServer.PollHostList().Length);
				HostData[] tempData = MasterServer.PollHostList();
				for(int i = 0; i < tempData.Length; i++){
					hostData.Add(tempData[i]);	
				}
			}
		}
		if(Input.GetKeyUp(KeyCode.Escape)){
			if(role == Role.Captain && GameObject.Find ("Captain").GetComponent<Captain>().zoomed){
				GameObject.Find("Captain").GetComponent<Captain>().MapZoom(ZoomDir.Out);
			}
			else
				cleanupAndDisconnect();
		}
		if (Network.isServer && playersReady == Network.connections.Length && role != Role.None && !gameStarted){
			boat.GetComponent<Boat>().SetSpeed(boat.GetComponent<Boat>().maxSpeed);
			gameStarted = true;
		}
	}
	
	public Role GetRole(){
		return role;
	}
}
