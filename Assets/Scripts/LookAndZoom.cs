﻿using UnityEngine;
using System.Collections;

public class LookAndZoom : MonoBehaviour {
	public float maxX;
	public float minX;
	public float maxY;
	public float minY;

	public float zRotation = -90;
	void Update () {
		float rotx = ClampAngle(-Input.GetAxisRaw("Vertical") + transform.rotation.eulerAngles.x, minX, maxX);
		float roty = ClampAngle(Input.GetAxisRaw("Horizontal") + transform.rotation.eulerAngles.y, minY, maxY);
		transform.rotation = Quaternion.Euler(new Vector3(rotx,
		                                                  roty, zRotation));
		if(Input.GetKey(KeyCode.LeftShift) && camera.fieldOfView > 20){
			camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, 20, Time.deltaTime * 5);
		}
		else if(camera.fieldOfView < 45){
			camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, 45, Time.deltaTime * 5);
		}
	}
	float ClampAngle (float angle, float min, float max) {
		if(angle > 180) angle = 360 - angle;
		angle = Mathf.Clamp(angle, min, max);
		if(angle < 0) angle = 360 + angle;
		return angle;
	}
}
