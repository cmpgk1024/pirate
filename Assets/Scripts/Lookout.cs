﻿using UnityEngine;
using System.Collections;

public class Lookout : MonoBehaviour {
	// Update is called once per frame
	Camera cam;
	void Start(){
		cam = transform.Find("Camera").camera;
	}
	void Update () {
		if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || 
		   Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow)) {
			cam.transform.rotation = Quaternion.Euler(new Vector3(-Input.GetAxisRaw("Vertical") + cam.transform.rotation.eulerAngles.x,
			 Input.GetAxisRaw("Horizontal") + cam.transform.rotation.eulerAngles.y, 0));
		}
		if(Input.GetKey(KeyCode.LeftShift)){
			Ray ray = new Ray(transform.position, cam.transform.forward);
			RaycastHit hit;
			if(Physics.Raycast(ray, out hit)){
				if(hit.transform.gameObject.tag == "WeakPoint"){
					hit.transform.gameObject.renderer.enabled = true;
				}
				else{
					foreach(GameObject wp in GameObject.FindGameObjectsWithTag("WeakPoint")){
						wp.renderer.enabled = false;
					}
				}
			}
		}
		if(Input.GetKeyUp(KeyCode.LeftShift)){
			foreach(GameObject wp in GameObject.FindGameObjectsWithTag("WeakPoint")){
				wp.renderer.enabled = false;
			}
		}
		if(Input.GetKey(KeyCode.LeftShift) && cam.fieldOfView > 20){
			cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, 20, Time.deltaTime * 5);
		}
		else if(cam.fieldOfView < 45){
			cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, 45, Time.deltaTime * 5);
		}
		/*if (Input.touchCount > 0) {
			transform.Rotate(Input.GetTouch(0).deltaPosition);
		}*/
	}
}
