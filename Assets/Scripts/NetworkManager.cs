﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class NetworkManager : MonoBehaviour {
	
	
	protected string gameName = "com.rainshapes.pirate";
	protected string userName = "Player";
	protected string serverName = "Player's Crew";
	protected bool refreshing;
	protected bool wasConnected;
	protected float btnX;
	protected float btnY;
	protected float btnH;
	protected float btnW;
	protected List<HostData> hostData = new List<HostData>();
	
	protected virtual void OnServerInitialized(){
		Debug.Log("Server initialized!");
	}
	
	protected virtual void OnConnectedToServer(){
	}
	
	protected virtual void OnPlayerDisconnected(NetworkPlayer player){
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}
	
	//actions
	
	protected void refreshHostList(){
		MasterServer.RequestHostList(gameName);
		refreshing = true;
	}
	
	protected void startServer(){
		Network.InitializeServer(256, 25001, !Network.HavePublicAddress());
		MasterServer.RegisterHost(gameName, serverName, "Pirate");	
	}
	
	protected void cleanupAndDisconnect(){
		/*if(Network.isServer){
			GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
			foreach(GameObject player in Players){
				Network.RemoveRPCs(player.networkView.viewID);
				Network.Destroy(player);
			}
		}
		else if(Network.isClient){
			GameObject[] Players = GameObject.FindGameObjectsWithTag("Player");
			foreach(GameObject player in Players){
				if(player.networkView.isMine){
					Network.RemoveRPCs(player.networkView.viewID);
					Network.Destroy(player);
				}
			}
		}*/ //this is deflect code
		Network.Disconnect();
		Application.LoadLevel("scene");
	}
	
	protected void Start(){
		btnX = Screen.width * 0.05f;
		btnY = Screen.width * 0.05f;
		btnW = Screen.width * 0.1f;
		btnH = Screen.width * 0.1f;
	}
	
	protected virtual void OnGUI(){
		if(!Network.isClient && !Network.isServer){
			Screen.showCursor = true;
			if(GUI.Button(new Rect(btnX, btnY, btnH, btnW), "Start Server")){
				startServer();
			}
			if(GUI.Button(new Rect(btnX, btnY * 1.1f + btnH, btnH, btnW), "Refresh hosts")){
				Debug.Log("Refreshing hosts.");
				wasConnected = false;
				refreshHostList();
			}
			if(hostData != null && !wasConnected){
				for(int i = 0; i < hostData.Count; i++){
					if(GUI.Button(new Rect(btnX * 1.5f + btnW, btnY*1.2f + (btnH * i), btnW * 3f, btnH * 0.5f), hostData[i].gameName)){
						Network.Connect(hostData[i]);
					}
				}
			}
			GUI.Label(new Rect(20f, (Screen.height/2 * 1.8f), 200f, 20f), "Name:");
			userName = GUI.TextField(new Rect(20f, (Screen.height/2f * 1.9f), 200f, 20f), userName);
			serverName = userName + "'s Crew";
		}
	}
	
	protected virtual void Update(){
		if(refreshing){
			if(MasterServer.PollHostList().Length > 0){
				refreshing = false;
				Debug.Log("Number of hosts: " + MasterServer.PollHostList().Length);
				HostData[] tempData = MasterServer.PollHostList();
				for(int i = 0; i < tempData.Length; i++){
					hostData.Add(tempData[i]);	
				}
			}
		}
		if(Input.GetKey(KeyCode.Escape)){
			cleanupAndDisconnect();
		}
	}
}