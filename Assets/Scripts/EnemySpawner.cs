﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {
	public GameObject Enemy;
	void Update () {
		if (GameObject.FindGameObjectsWithTag ("Enemy").Length < 1 && Network.isServer)
						Network.Instantiate (Enemy, transform.position, Quaternion.identity, 0);
	}
}
