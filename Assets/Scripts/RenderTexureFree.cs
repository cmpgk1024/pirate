﻿using UnityEngine;
using System.Collections;

public class RenderTexureFree : MonoBehaviour {
	
	public Texture2D target;
	public float captureRate = 5;
	float nextCapture = 0;
	
	void Start(){
		camera.aspect = 1f; // this is so it renders cleanly onto our quad regardless of screen size
	}
	
	public IEnumerator Capture(){
		yield return new WaitForEndOfFrame();
		Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, true);
		texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
		texture.Apply();
		target = texture;
	}
	void OnPostRender(){
		if(Time.time > nextCapture){
			Texture2D texture = new Texture2D (Screen.width, Screen.height, TextureFormat.RGB24, true);
			texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
			texture.Apply();
			target = texture;
			nextCapture = Time.time + captureRate;
		}
	}
}
