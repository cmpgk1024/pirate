﻿using UnityEngine;
using System.Collections;

public class Boat : MonoBehaviour {
	float Speed = 0;
	public float maxSpeed = 15;
	int Health = 100;
	void Update () {
		rigidbody.velocity = transform.right * Speed;
		if (Health == 0)
			OnDeath ();
	}
	void OnCollisionEnter(Collision c){
		if (c.gameObject.tag == "Land") {
			Debug.Log ("Game over");
			Health = 0;
		}
	}
	void OnDeath(){
		SetSpeed(0);
		Role role = GameObject.Find ("NetworkManager").GetComponent<PirateNetworkManager>().GetRole();
		if(role != Role.Captain)
			transform.Find (role.ToString()).gameObject.SetActive(false);
		else
			GameObject.Find ("CaptainRoot").transform.Find("Captain").gameObject.SetActive(false);
		transform.Find ("DeathCam").gameObject.SetActive(true);
	}

	public void Steer(Vector3 angle){
		transform.Rotate (angle);
		networkView.RPC("SteerRPC", RPCMode.Others, angle);
	}

	public void SetSpeed(float s){
		Speed = s;
		networkView.RPC ("SetSpeedRPC", RPCMode.OthersBuffered, s);
	}

	[RPC]
	void SetSpeedRPC(float s){
		Speed = s;
	}
	
	[RPC]
	void SteerRPC(Vector3 angle){
		transform.Rotate(angle);
	}
}
