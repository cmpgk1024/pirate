﻿using UnityEngine;
using System.Collections;

public class PositionLock : MonoBehaviour {

	public Vector3 position;

	// Update is called once per frame
	void Update () {
		transform.position = position;
	}
}
